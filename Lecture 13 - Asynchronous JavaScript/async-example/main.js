function fetchJSON(path, callback) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', path, true);
    xhr.onload = function() {
        if (this.status === 200) {
            const json = JSON.parse(this.responseText);
            callback(null, json);
        } else {
            callback(new Error(this.statusText), null);
        }
    }
    xhr.send();
}

function fetchJSONAsync(path) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', path, true);
        xhr.onload = function() {
            if (this.status === 200) {
                const json = JSON.parse(this.responseText);
                resolve(json);
            } else {
                reject(new Error(this.statusText));
            }
        }
        xhr.send();
    });
}

fetchJSON('users.json', (error, usersJson) => {
    if (error) {
        console.error(error);
    } else {
        fetchJSON('projects.json', (error, projectsJson) => {
            if (error) {
                console.error(error);
            } else {
                console.log(usersJson);
                console.log(projectsJson);
            }
        });
    }
})

fetchJSONAsync('users.json')
    .then((usersJson) => {
        fetchJSONAsync('projects.json')
            .then((projectsJson) => {

            })
    })

Promise.all([
    fetchJSONAsync('users.json'),
    fetchJSONAsync('projects.json'),
]).then(([usersJson, projectsJson]) => {
    console.log(usersJson);
    console.log(projectsJson);
}).catch(console.error);

(async function() {
    try {
        const [
            usersJson,
            projectsJson,
        ] = await Promise.all([
            fetchJSONAsync('users.json'),
            fetchJSONAsync('projects.json'),
        ]);
        console.log(usersJson);
        console.log(projectsJson);
    } catch(error) {
        console.error(error);
    }
})();

// fetchJSONAsync('projects.json')
//     .then((json) => console.log(json))
//     .catch((error) => console.error(error));


// async function readJSON() {
//     try {
//         const json = await fetchJSONAsync('projects.json');
//         console.log(json);
//     } catch(error) {
//         console.error(error);
//     }
// }
// readJSON();
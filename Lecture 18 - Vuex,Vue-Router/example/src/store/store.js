import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    count: 0,
  },
  mutations: {
    changeBy(state, value) {
      state.count += value;
    }
  },
  actions: {
    change(context, value) {
      if(typeof value == 'number') {
        context.commit('changeBy', value);
      } else {
        alert('Error!');
      }
    }
  },
});


export {
  store
};
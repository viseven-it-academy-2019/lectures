import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Home from './components/Home';
import About from './components/About';
import Items from './components/Items';
import { store } from './store/store';

Vue.config.productionTip = false

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/items/:id', component: Items, props: true },
  ]
});

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')

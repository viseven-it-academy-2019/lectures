window.addEventListener('load', function(){
	var profileForm = document.getElementsByName('profile')[0],
		profileOutput = document.getElementsByName('profileOutput')[0],
		result = document.getElementById('result'),
		nameElement = document.getElementById('userName'),
		nameReg = /co+l!*/gim;

	nameElement.addEventListener('input', function(){
		console.log(nameReg.test(nameElement.value));
	});

	profileForm.addEventListener('submit', function(event){
		var data = getFormData();
		event.preventDefault();
		printOutput(data);
	});

	profileForm.addEventListener('reset', function(){
		clearOutput();
	});

	function getFormData(){
		return {
			name: profileForm.userName.value,
			surname: profileForm.userSurname.value,
			email: profileForm.userEmail.value,
			sex: profileForm.userSex.value,
			avatar: profileForm.userAvatar.value,
			destiny: profileForm.userDestiny.value,
			agreement: profileForm.userAgreement.checked
		};
	}

	function printOutput(data){
		profileOutput.innerHTML = JSON.stringify(data);
		result.classList.add('active');
	}

	function clearOutput(){
		profileOutput.innerHTML = '';
		result.classList.remove('active');
	}
});
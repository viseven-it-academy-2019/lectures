function getJson(){
	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'phones.json', true);

	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;

		if (this.status != 200) {
			console.log(this.status + ': ' + this.statusText);
		} else {
			console.log(this.responseText);
		}

	};

	xhr.send();
}

function sendGetParams(){
	var xhr = new XMLHttpRequest();

	xhr.open('GET', '/test?a=1&b="some_test_string"', true);

	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;

		if (this.status != 200) {
			console.log(this.status + ': ' + this.statusText);
		} else {
			console.log(this.responseText);
		}

	};

	xhr.send();
}

function sendPostParams(){
	var xhr = new XMLHttpRequest();

	xhr.open('POST', '/test', true);

	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;

		if (this.status != 200) {
			console.log(this.status + ': ' + this.statusText);
		} else {
			console.log(this.responseText);
		}

	};

	xhr.send({ someNumber: 1 });
}
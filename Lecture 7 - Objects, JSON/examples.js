var gallery = {
  img1: {
    name: 'Cat',
    path: './cat.jpg',
    date: new Date(),
  },
  img2: {
    name: 'Dog',
    path: './dog.jpg',
    date: new Date(),
  },
  img3: {
    name: 'Cow',
    path: './cow.jpg',
    date: new Date(),
  },
};

// 1.
// console.log(JSON.stringify(gallery));

// 2
console.log(JSON.stringify(gallery, (key, value) => {
  if (!key || key === 'name' || key.indexOf('img') > -1) {
    return value;
  }
}, 4));

var gallery = {
  img1: {
    name: 'Cat',
    path: './cat.jpg',
    date: new Date(),
  },
  img2: {
    name: 'Dog',
    path: './dog.jpg',
    date: new Date(),
  },
  img4: {
    name: '',
    path: './dog.jpg',
    date: new Date(),
  },
  img3: {
    name: 'Cow',
    path: './cow.jpg',
    date: new Date(),
  },
};
for (key in gallery) {
  if (gallery.hasOwnProperty(key)) {
    gallery[key].toJSON = function () {
      if (this.name) {
        return this;
      }
    };
  }
}
console.log(json = JSON.stringify(gallery, null, 4));

g1 = JSON.parse(json, (key, value) => {
  if (key === 'date') {
    return new Date(value);
  }
  return value;
});

console.log(g1);

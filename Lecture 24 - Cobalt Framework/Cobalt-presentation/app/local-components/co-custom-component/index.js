var createComponent = require('component').createComponent,
	CustomComponent = require('./src/js/custom-component').CustomComponent;

module.exports = createComponent({
	name: 'co-custom-component',
	constructor: CustomComponent,
	localizationTemplate: resolve('i18n/{lang}.json'),
	localExtensions: {
		components: {
			"co-slider": require("co-slider")
		}
	}
});

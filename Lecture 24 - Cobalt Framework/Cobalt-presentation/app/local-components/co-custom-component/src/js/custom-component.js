import {Component} from 'component';
import coSlider from 'co-slider'

export class CustomComponent extends Component {
    constructor() {
        super(...arguments);
        this.counter = 0
    }

    ready(view) {
        this.style = 'color:blue;'
    }

    get defaults() {
        return {
            'text': {
                type: 'text',
                value: 'Default property value',
                refresh: true
            },
            'sliderModel':{
                type: Object,
                value:{
                  'value':0
                },
                base: coSlider.constructor,
                scheme: coSlider.constructor.prototype.defaults
            }
        };
    }

    refresh(prop) {
        switch (prop) {
            case 'text':
                this.changeColor();
                break
        }
    }

    clickHandler(event, view) {
        view.counter++;
        view.text = `was clicked ${view.counter}`
    }

    changeColor() {
        const hexColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
        this.style = `color:${hexColor};`
    }

    activate(targetEl,view) {
        view.changeColor()
    }

    deactivate(targetEl, view) {
        view.changeColor()
    }

    template() {
        return require('../template.html');
    }
}

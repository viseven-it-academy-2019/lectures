(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (resolve){
'use strict';

var createComponent = require('component').createComponent,
    CustomComponent = require('./src/js/custom-component').CustomComponent;

module.exports = createComponent({
	name: 'co-custom-component',
	constructor: CustomComponent,
	localizationTemplate: resolve('i18n/{lang}.json'),
	localExtensions: {
		components: {
			"co-slider": require("co-slider")
		}
	}
});
require("rivets").components[module.exports.name] = module.exports;

}).call(this,function(relativePath){
			var settings = require('settings'),
				componentsDirecotry = settings.components,
				emptyString = '';

			  return componentsDirecotry.replace(new RegExp('/$'), emptyString) + '/co-custom-component/' + relativePath.replace(new RegExp('^/'), emptyString);
		})
},{"./src/js/custom-component":16,"co-slider":6,"component":10,"rivets":"rivets","settings":"settings"}],2:[function(require,module,exports){
(function (resolve){
'use strict';

var createComponent = require('component').createComponent,
    Container = require('./src/js/Container.js').Container;

module.exports = createComponent({
	name: 'co-container',
	constructor: Container,
	localizationTemplate: resolve('i18n/{lang}.json')
});

}).call(this,function(relativePath){
			var settings = require('settings'),
				componentsDirecotry = settings.components,
				emptyString = '';

			  return componentsDirecotry.replace(new RegExp('/$'), emptyString) + '/co-custom-component/_assets/co-container/' + relativePath.replace(new RegExp('^/'), emptyString);
		})
},{"./src/js/Container.js":3,"component":10,"settings":"settings"}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Container = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _component = require("component");

var _dispatcher = require("dispatcher");

var _dispatcher2 = _interopRequireDefault(_dispatcher);

var _utils = require("./utils.js");

var localUtils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HIDDEN_ELEMENT = 'hidden-element';
var HIGHLIGHTED_CLASS = 'highlighted';
var EMPTY_CLASS = 'empty';

var Container = exports.Container = function (_Component) {
	_inherits(Container, _Component);

	function Container() {
		_classCallCheck(this, Container);

		var _this = _possibleConstructorReturn(this, (Container.__proto__ || Object.getPrototypeOf(Container)).apply(this, arguments));

		if (_this.element.getAttribute('container') === 'false') {
			_this.element.removeAttribute('container');
		} else {
			_this.element.setAttribute('container', 'true');
		}

		_this.subscribeAction = _dispatcher2.default.subscribeAction(function (action) {
			console.log(action.type);
			if (action.type === _dispatcher2.default.ActionType.EDIT_PRESENTATION) {
				//element.classList.add(HIGHLIGHTED_CLASS)
				_this.element.classList.add(HIGHLIGHTED_CLASS);
			} else if (action.type === _dispatcher2.default.ActionType.VIEW_PRESENTATION) {
				//element.classList.remove(HIGHLIGHTED_CLASS)
				_this.element.classList.remove(HIGHLIGHTED_CLASS);
			}
		});

		_this.addEmptyClass();
		localUtils.observe(_this.element, { childList: true, subtree: true }, _this.addEmptyClass.bind(_this));
		return _this;
	}

	_createClass(Container, [{
		key: "getChilds",
		value: function getChilds() {
			var children = this.element.children;

			return Array.from(children).filter(function (elem) {
				return elem.model;
			}).filter(function (elem) {
				return !elem.classList.contains(HIDDEN_ELEMENT);
			});
		}
	}, {
		key: "addEmptyClass",
		value: function addEmptyClass() {
			this.element.classList[!this.getChilds().length ? 'add' : 'remove'](EMPTY_CLASS);
		}
	}, {
		key: "unbind",
		value: function unbind() {
			this.subscribeAction.unsubscribe();
		}
	}, {
		key: "template",
		value: function template() {
			return require("../template.html");
		}
	}, {
		key: "defaults",
		get: function get() {
			return {};
		}
	}]);

	return Container;
}(_component.Component);

},{"../template.html":5,"./utils.js":4,"component":10,"dispatcher":"dispatcher"}],4:[function(require,module,exports){
'use strict';

var _utils = require('utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

module.exports.observe = function (element, config, callback) {
	new MutationObserver(function (mutations) {
		callback(mutations);
	}).observe(element, config);
	callback([]);
};

},{"utils":"utils"}],5:[function(require,module,exports){
module.exports = '<content co-container></content>'
},{}],6:[function(require,module,exports){
(function (resolve){
'use strict';

var createComponent = require('component').createComponent,
    Slider = require('./src/js/slider.js').Slider,
    rivets = require('rivets');

module.exports = createComponent({
	name: 'co-slider',
	constructor: Slider,
	localExtensions: {
		components: {
			'co-container': require('co-container')
		}
	},
	localizationTemplate: resolve('i18n/{lang}.json')
});

}).call(this,function(relativePath){
			var settings = require('settings'),
				componentsDirecotry = settings.components,
				emptyString = '';

			  return componentsDirecotry.replace(new RegExp('/$'), emptyString) + '/co-custom-component/_assets/co-slider/' + relativePath.replace(new RegExp('^/'), emptyString);
		})
},{"./src/js/slider.js":7,"co-container":2,"component":10,"rivets":"rivets","settings":"settings"}],7:[function(require,module,exports){
'use strict';

var touch = require('touch'),
    utils = require('utils'),
    rivets = require('rivets'),
    prefixer = require('prefixer'),
    Component = require('component').Component;

rivets.binders.visible = function (element, value) {
	if (!value) {
		element.style.visibility = 'hidden';
	} else {
		element.style.visibility = '';
	}
};

require('co-container');

utils.inherits(Slider, Component);

function Slider(element) {
	Component.apply(this, Array.prototype.slice.call(arguments));

	this.filler = this.element.querySelector('.slider-fill');

	['value', 'disabled'].forEach(function (property) {
		Object.defineProperty(this.element, property, {
			get: function () {
				return this[property];
			}.bind(this),
			set: function (newValue) {
				this[property] = newValue;
			}.bind(this)
		});
	}, this);
}

Slider.prototype.template = function () {
	return require('../template.html');
};

Slider.prototype.ready = function (view) {
	view.addBinding(this.element, 'class', 'orientation');
	view.addBinding(this.element, 'class-reverse', 'reverse');
	this.checkDisabled();
	this.refresh();
};

Slider.prototype.defaults = {
	min: {
		type: Number,
		value: 0,
		refresh: true
	},
	max: {
		type: Number,
		value: 100,
		refresh: true
	},
	step: {
		type: Number,
		value: 1,
		refresh: true
	},
	value: {
		type: Number,
		value: 0,
		refresh: true
	},
	fill: {
		type: Boolean,
		value: true,
		refresh: true,
		optional: true
	},
	reverse: {
		type: Boolean,
		value: false,
		refresh: true,
		optional: true
	},
	orientation: {
		type: String,
		value: 'horizontal',
		values: ['horizontal', 'vertical'],
		refresh: true
	},
	disabled: {
		type: Boolean,
		value: false,
		refresh: true
	}
};

Slider.prototype.refresh = function (attr) {
	var isNegMax = this.max < this.min;
	if (attr) {
		this[attr] = utils.reviveByType(this[attr], this.defaults[attr].type);
	}

	switch (attr) {
		case 'disabled':
			this.checkDisabled();
			break;

		case 'value':
			this.element.value = this.value;
			this.redraw();
			break;

		case 'max':
			if (isNegMax) {
				this.min = this.max;
			}
			if (this.value > this.max) {
				this.value = this.max;
			}
			this.redraw();
			break;
		case 'min':
			if (isNegMax) {
				this.max = this.min;
			}
			if (this.value < this.min) {
				this.value = this.min;
			}
			this.redraw();
			break;
		case 'step':
			if (this[attr] <= 0) {
				this[attr] = 1;
			}
			break;
		case 'orientation':
			this.filler.style.width = this.filler.style.height = 'auto';
			this.redraw();
			break;
		default:
			this.redraw();
			break;
	}

	utils.dispatchEvent(this.element, 'refresh', { detail: attr });
};

Slider.prototype.handleEvent = function (event) {
	var touchesCount = event.touches ? event.touches.length : 1;

	event.stopPropagation();

	if (touchesCount < 2) {
		switch (event.type) {
			case touch.events.start:
				this.focus();

			case touch.events.move:
				event.preventDefault();
				event = touch.getOriginalEvent(event);
				this.processEvent(event);
				utils.dispatchEvent(this.element, 'change');
				break;

			case touch.events.end:
				this.blur();
				break;
		}
	}
};

Slider.prototype.checkDisabled = function () {
	this.disabled ? this.disable() : this.enable();
};

Slider.prototype.enable = function () {
	this.element.addEventListener(touch.events.start, this);
};

Slider.prototype.disable = function () {
	this.element.removeEventListener(touch.events.start, this);
};

Slider.prototype.focus = function () {
	document.addEventListener(touch.events.move, this);
	document.addEventListener(touch.events.end, this);
	utils.dispatchEvent(this.element, 'focus');
};

Slider.prototype.blur = function () {
	document.removeEventListener(touch.events.move, this);
	document.removeEventListener(touch.events.end, this);
	utils.dispatchEvent(this.element, 'blur');
};

Slider.prototype.processEvent = function (event) {
	var position,
	    value,
	    rect = this.element.getBoundingClientRect();

	if (this.orientation == 'horizontal') {
		position = (event.pageX - rect.left) / rect.width;
	} else {
		position = (event.pageY - rect.top) / rect.height;
	}

	if (this.reverse) {
		position = 1 - position;
	}

	this.value = (this.max - this.min) * position + this.min;
};

Slider.prototype.redraw = function () {
	this.value = this.getValidValue(this.value);
	this.position = this.getPositionFromValue(this.value);
	this.draw();
};

Slider.prototype.getValidValue = function (value) {
	value = utils.rangeValue(value, this.min, this.max);
	if (value >= this.max) {
		return this.max;
	}

	value = Math.round((value - this.min) / this.step) * this.step + this.min;
	value = this.normalizeFloatNumber(value);

	return value;
};

Slider.prototype.getPositionFromValue = function (value) {
	if (this.max === this.min) {
		return 0;
	}
	return (value - this.min) / (this.max - this.min);
};

Slider.prototype.draw = function () {
	var property = this.orientation === 'horizontal' ? 'width' : 'height';
	this.filler.style[property] = this.position * 100 + '%';
};

Slider.prototype.normalizeFloatNumber = function (value) {
	var incremented = this.step + 1,
	    // '1 + ' - fix to avoid scientific notation after convert to string
	split = incremented.toString().split('.'),
	    symbolsNumber = split[1] ? split[1].length : 0;

	return parseFloat(value.toFixed(symbolsNumber));
};

exports.Slider = Slider;

},{"../template.html":8,"co-container":2,"component":10,"prefixer":"prefixer","rivets":"rivets","touch":"touch","utils":"utils"}],8:[function(require,module,exports){
module.exports = '<co-container container="false" fixed="size, position, animation, action" class="slider" user-label="Slider content" co-slider><co-container container="false" fixed="size, position, animation, action" class="slider-fill" rv-visible="fill" user-label="Slider fill" data-prevent-right-swipe="true" data-prevent-left-swipe="true" co-slider></co-container><div class="slider-handler-wrapper" co-slider><co-container container="false" fixed="size, position, animation, action" class="slider-handler" user-label="Slider handler" data-prevent-right-swipe="true" data-prevent-left-swipe="true" co-slider><content co-slider></content></co-container></div></co-container>'
},{}],9:[function(require,module,exports){
module.exports={
	"localizationPath": "components/{name}/i18n/{lang}.json"
}
},{}],10:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var rivets = require('rivets');
var utils = require('utils');
var ComponentConstructor = require('./src/js/ComponentConstructor.js').ComponentConstructor;
var buildModel = require('./src/js/buildModel.js');
var buildObservations = require('./src/js/buildObservations.js');
var config = require('./config.json');

function registerComponent(name, Constructor, localExtensions) {
  rivets.components[name] = createComponent(name, Constructor, localExtensions);
}

function createComponent(options, Constructor, localExtensions) {
  var name;

  if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
    name = options.name;
    Constructor = options.constructor;
    localExtensions = options.localExtensions;
  } else {
    name = options;
  }

  Constructor.prototype.localizationTemplate = options.localizationTemplate || config.localizationPath;

  var descriptor = {
    name: name,
    block: Constructor.block,
    static: getStaticProperties(Constructor.prototype.defaults),
    template: Constructor.prototype.template,
    constructor: Constructor,
    initialize: function initialize(element, data) {
      var defaultProperties = utils.toCamelCaseObject(Constructor.prototype.defaults);
      var model = buildModel(defaultProperties, data, name);
      var componentInstance = new Constructor(element, model, this.view, this);

      Constructor.prototype.name = name;

      buildObservations.call(this, componentInstance, defaultProperties, data.model || {});

      return componentInstance;
    },
    unbind: function unbind() {
      // TODO: make unobserve..
    }
  };

  if (localExtensions) {
    rivets._.extensions.forEach(function (extensionName) {
      var extension = localExtensions[extensionName];

      if (extension) {
        descriptor[extensionName] = extension;
      }
    });
  }

  return descriptor;
}

function getStaticProperties(properties) {
  return Object.keys(properties).filter(function (property) {
    return isStaticProperty(properties, property);
  }).map(function (property) {
    return utils.toCamelCase(property);
  });
}

function isStaticProperty(properties, property) {
  return properties[property] && properties[property].static;
}

module.exports = registerComponent;
module.exports.createComponent = createComponent;
module.exports.Component = ComponentConstructor;

},{"./config.json":9,"./src/js/ComponentConstructor.js":11,"./src/js/buildModel.js":14,"./src/js/buildObservations.js":15,"rivets":"rivets","utils":"utils"}],11:[function(require,module,exports){
'use strict';

var _Promise = typeof Promise === 'undefined' ? require('es6-promise').Promise : Promise;

var utils = require('utils');
var builKeypath = require('./buildKeypath.js');
var getLocalizationByConstructor = require('./buildLocalization.js');
var generalModel = require('general-model');
var dispatcher = require('dispatcher');

function ComponentConstructor(element, model, view, binding) {
  this.element = element;
  this.parentScope = view.models;
  utils.mixin(this, model);
  this.element.model = this;
  this.element.view = {
    subscribe: subscribeToView.bind(this)
  };
  this.element.setAttribute('co-component', '');
  this.model = model;
  this.view = view;
  this.binding = binding;
  this.element.addEventListener('activate', handleActivation.bind(this));
  this.element.addEventListener('deactivate', handleDeactivation.bind(this));

  this.setIsConfigurable();
}

ComponentConstructor.prototype.isCommon = function () {
  return isCommonModel(this) || isCommonBinders(this);
};

ComponentConstructor.prototype.getTextElementsByProperty = function (property) {
  return utils.toArray(this.element.querySelectorAll('[rv-html]')).filter(function (element) {
    return element.getAttribute('rv-html') === property;
  }).filter(function (element) {
    return element.scope === this;
  }, this);
};

ComponentConstructor.prototype.getLocalization = function (lang) {
  return getLocalizationByConstructor(this.__proto__.constructor, lang);
};

ComponentConstructor.prototype.getAbsoluteKeypath = function () {
  return builKeypath(this.parentScope, this.getKeypath());
};

ComponentConstructor.prototype.getKeypath = function () {
  return this.element.getAttribute('model') || '';
};

ComponentConstructor.prototype.toJSON = function () {
  var that = this;
  return Object.keys(this).reduce(function (acc, property) {
    if (that.model.hasOwnProperty(property)) {
      acc[property] = that[property];
    }
    return acc;
  }, {});
};

ComponentConstructor.prototype.setIsConfigurable = function () {
  var keypath = this.getKeypath();
  Object.defineProperty(this, 'isConfigurable', {
    value: utils.startsWith(keypath, 'm.'),
    enumerable: true,
    configurable: false,
    writable: true
  });
};

ComponentConstructor.prototype.getLabel = function (lang) {
  return new _Promise(function (resolve, reject) {
    var label = getLabelFromElement(this.element);

    if (label) {
      resolve(label);
    } else {
      getLocalizationByConstructor(this.__proto__.constructor, lang).then(function (localization) {
        resolve(localization.name);
      }).catch(function (err) {
        resolve(getNameFromTagNameComponent(this.name || this.element.tagName.toLowerCase()));
      }.bind(this));
    }
  }.bind(this));
};

ComponentConstructor.prototype.setLabel = function (label) {
  this.element.setAttribute('user-label', label);
};

ComponentConstructor.prototype.subscribe = function (callback) {
  return new generalModel.ModelNotifier().observe(this.model).subscribe(function (model) {
    callback(model);
  }, true);
};

function getNameFromTagNameComponent(coTagName) {
  var normalName = coTagName.replace('co-', '').split('-').join(' ');

  return normalName[0].toUpperCase() + normalName.substring(1, normalName.lenght);
}

function handleActivation(event) {
  if (typeof this.activate === 'function' && isCobaltEvent(event)) {
    var prevModelState = utils.parse(this);
    this.activate(event.target);
    this.difference = utils.difference(prevModelState, utils.parse(this));
  }
}

function handleDeactivation(event) {
  if (typeof this.deactivate === 'function' && isCobaltEvent(event)) {
    this.deactivate(event.target);
  }

  utils.deepMixin(this, this.difference);
}

function isCobaltEvent(event) {
  return event.detail && !!event.detail.isCobaltEvent;
}

function isCommonModel(scope) {
  return getParentKeypathes(scope).some(function (keypath) {
    return isCommonKeypath(keypath);
  });
}

function isCommonBinders(scope) {
  var binders = scope.element && scope.element.binders;
  return binders && Object.keys(binders).some(function (binder) {
    return isCommonKeypath(binders[binder].keypath);
  });
}

function isCommonKeypath(keypath) {
  return utils.startsWith(keypath, 'm.common');
}

function getParentKeypathes(scope, keypathes) {
  keypathes = keypathes || [];
  if (scope && typeof scope.getKeypath === 'function') {
    keypathes.push(scope.getKeypath());
    getParentKeypathes(scope.parentScope, keypathes);
  }

  return keypathes;
}

function getLabelFromElement(element) {
  return element.getAttribute('user-label');
}

function subscribeToView(callback) {
  var _this = this;

  dispatcher.subscribeEvent(function (event) {
    if (event.type === dispatcher.EventType.DOM_REFRESHED && event.element === _this.element) {
      callback(_this.element);
    }
  });
}

exports.ComponentConstructor = ComponentConstructor;

},{"./buildKeypath.js":12,"./buildLocalization.js":13,"dispatcher":"dispatcher","es6-promise":"es6-promise","general-model":"general-model","utils":"utils"}],12:[function(require,module,exports){
'use strict';

function buildAbsoluteKeypath(parentPath, keypath) {
	var key = removeAlias(keypath);

	if (parentPath) {
		return key ? parentPath + '.' + removeAlias(keypath) : parentPath;
	} else {
		return keypath;
	}
}

function collectParentsKeypath(model, keypathes) {
	keypathes = keypathes || [];
	if (model && model.keypath) {
		keypathes.unshift(keypathModifier(model), model.index);
		collectParentsKeypath(model.__proto__, keypathes);
	}

	return keypathes;
}

function keypathModifier(model) {
	if (model.__proto__.keypath) {
		return removeAlias(model.keypath);
	}

	return model.keypath;
}

function removeAlias(keypath) {
	return keypath.split('.').slice(1).join('.');
}

module.exports = function (parentScope, keypath) {
	return buildAbsoluteKeypath(collectParentsKeypath(parentScope).join('.'), keypath);
};

},{}],13:[function(require,module,exports){
'use strict';

var _loader = require('loader');

var _utils = require('utils');

var _cache = require('cache');

var _cache2 = _interopRequireDefault(_cache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _Promise = typeof Promise === 'undefined' ? require('es6-promise').Promise : Promise;

var cache = new _cache2.default();

module.exports = getLocalizationByConstructor;

function getLocalizationByConstructor(constructor, outsideLang) {
  var defaultLang = 'en';
  var lang = outsideLang || defaultLang;
  // TODO: works only if localizationTemplate and name were added to the prototype during component registration

  return getLocalization(constructor, lang, defaultLang).then(function (localization) {
    return extendLocalizationByScheme(localization.defaults, constructor.prototype.defaults, lang).then(function () {
      return localization;
    });
  });
}

function getLocalization(constructor, lang, defaultLang) {
  var localizationPath = getLocalizationPath(constructor, lang);

  return loadLocalizationData(localizationPath).then(function (localization) {
    if (isLocalization(localization)) {
      return localization;
    } else if (lang !== defaultLang) {
      var defaultLocalizationPath = getLocalizationPath(constructor, defaultLang);
      return loadLocalizationData(defaultLocalizationPath);
    }
    return _Promise.resolve({});
  });
}

function getLocalizationPath(constructor, lang) {
  return (0, _utils.template)(constructor.prototype.localizationTemplate, { name: constructor.prototype.name, lang: lang });
}

function loadLocalizationData(path) {
  var loadFromCache = true;
  var reviver = null;

  if (!cache.isCache(path)) {
    cache.set(path, (0, _loader.loadJSON)(path, reviver, loadFromCache));
  }

  return cache.get(path).then(function (data) {
    return Object.assign({}, data);
  });
}

function isLocalization(localization) {
  return localization && Object.keys(localization).length;
}

// if property described in scheme has 'base' defined, it's localization is extended with localization from 'base'
function extendLocalizationByScheme(localization, scheme, lang) {
  var localizationPromises = [];

  var _loop = function _loop(prop) {
    var propertyDescriptor = scheme[prop],
        localizationLoadingPromise = void 0;

    if (propertyDescriptor.sealed) {
      return 'continue';
    }

    if (propertyDescriptor.base) {
      localizationLoadingPromise = getLocalizationByConstructor(propertyDescriptor.base, lang).then(function (subLocalization) {
        return localization[prop].defaults = subLocalization.defaults;
      });
    } else if (propertyDescriptor.scheme) {
      localizationLoadingPromise = extendLocalizationByScheme(localization[prop].defaults, propertyDescriptor.scheme, lang);
    } else {
      return 'continue';
    }
    localizationPromises.push(localizationLoadingPromise);
  };

  for (var prop in scheme) {
    var _ret = _loop(prop);

    if (_ret === 'continue') continue;
  }

  return _Promise.all(localizationPromises);
}

},{"cache":"cache","es6-promise":"es6-promise","loader":"loader","utils":"utils"}],14:[function(require,module,exports){
"use strict";

var utils = require("utils");

function buildModel(defaultProperties, data, name) {
	var defaultModel = getDefaultModel(defaultProperties),
	    elementModel = utils.toCamelCaseObject(data.model || {}),
	    attributesData = getDataFromAttributes(defaultProperties, data),
	    model = mixin(mixin(defaultModel, elementModel, defaultProperties), attributesData, defaultProperties);

	return model;
}

function mixin(target, source, defaultProperties) {
	Object.keys(source).forEach(function (property) {
		if (source.hasOwnProperty(property) && defined(source[property]) && defaultProperties.hasOwnProperty(property)) {
			if (target[property] && target[property].constructor === Object) {
				addMissingProperies(defaultProperties[property].value, source[property]); // add missing properties and keep reference for correct observing
			}
			target[property] = source[property];
		}
	});

	return target;
}

function addMissingProperies(defaultProperties, source) {
	return Object.keys(defaultProperties).forEach(function (property) {
		if (!source.hasOwnProperty(property)) {
			source[property] = defaultProperties[property];
		}
	});
}

function getDefaultModel(defaultProperties) {
	return Object.keys(defaultProperties).reduce(function (acc, property) {
		acc[property] = defaultProperties[property].value;
		return acc;
	}, {});
}

function getDataFromAttributes(defaultProperties, data) {
	return Object.keys(defaultProperties).reduce(function (acc, property) {
		if (data.hasOwnProperty(property)) {
			if (defaultProperties[property] && defaultProperties[property].static) {
				acc[property] = utils.revive(data[property]);
			} else {
				acc[property] = data[property];
			}
		}
		return acc;
	}, {});
}

function defined(value) {
	return value !== undefined && value !== null;
}

module.exports = buildModel;

},{"utils":"utils"}],15:[function(require,module,exports){
'use strict';

var generalModel = require('general-model');
var complexProperties = [Object, Array, DataView, 'Object', 'Array', 'DataView'];

module.exports = function buildObservations(componentInstance, defaultProperties, model) {
  Object.keys(defaultProperties).forEach(function (property) {
    model[property] = componentInstance[property];

    if (isRefreshableProperty(defaultProperties, property) && isComplexProperty(defaultProperties, property)) {
      observeComplexProperty(property, componentInstance);
    }

    this.observe(componentInstance, property, function () {
      if (isRefreshableProperty(defaultProperties, property)) {
        componentInstance.refresh(property);
      }
      componentInstance.model[property] = componentInstance[property];
      model[property] = componentInstance[property];
    });

    this.observe(componentInstance.model, property, function () {
      componentInstance[property] = componentInstance.model[property];
    });

    this.observe(model, property, function () {
      componentInstance[property] = model[property];
    });
  }, this);
};

function observeComplexProperty(property, componentInstance) {
  // setTimeout because nested bindings...
  setTimeout(function () {
    new generalModel.ModelNotifier().deepObserve(componentInstance, property).subscribe(function (model) {
      componentInstance.refresh(property);
    }, true);
  }, 50);
}

function isComplexProperty(defaultProperties, property) {
  var isComplexProp = complexProperties.indexOf(defaultProperties[property].type) !== -1;
  return !isComplexProp ? Array.isArray(defaultProperties[property].type) : isComplexProp;
}

function isRefreshableProperty(defaultProperties, property) {
  return !!defaultProperties[property].refresh;
}

},{"general-model":"general-model"}],16:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.CustomComponent = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _component = require('component');

var _coSlider = require('co-slider');

var _coSlider2 = _interopRequireDefault(_coSlider);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CustomComponent = exports.CustomComponent = function (_Component) {
    _inherits(CustomComponent, _Component);

    function CustomComponent() {
        _classCallCheck(this, CustomComponent);

        var _this = _possibleConstructorReturn(this, (CustomComponent.__proto__ || Object.getPrototypeOf(CustomComponent)).apply(this, arguments));

        _this.counter = 0;
        return _this;
    }

    _createClass(CustomComponent, [{
        key: 'ready',
        value: function ready(view) {
            this.style = 'color:blue;';
        }
    }, {
        key: 'refresh',
        value: function refresh(prop) {
            switch (prop) {
                case 'text':
                    this.changeColor();
                    break;
            }
        }
    }, {
        key: 'clickHandler',
        value: function clickHandler(event, view) {
            view.counter++;
            view.text = 'was clicked ' + view.counter;
        }
    }, {
        key: 'changeColor',
        value: function changeColor() {
            var hexColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
            this.style = 'color:' + hexColor + ';';
        }
    }, {
        key: 'activate',
        value: function activate(targetEl, view) {
            view.changeColor();
        }
    }, {
        key: 'deactivate',
        value: function deactivate(targetEl, view) {
            view.changeColor();
        }
    }, {
        key: 'template',
        value: function template() {
            return require('../template.html');
        }
    }, {
        key: 'defaults',
        get: function get() {
            return {
                'text': {
                    type: 'text',
                    value: 'Default property value',
                    refresh: true
                },
                'sliderModel': {
                    type: Object,
                    value: {
                        'value': 0
                    },
                    base: _coSlider2.default.constructor,
                    scheme: _coSlider2.default.constructor.prototype.defaults
                }
            };
        }
    }]);

    return CustomComponent;
}(_component.Component);

},{"../template.html":17,"co-slider":6,"component":10}],17:[function(require,module,exports){
module.exports = '<p rv-html="text" rv-style="style" rv-on-click="clickHandler" co-custom-component></p><co-slider class="default" model="sliderModel" rv-value="counter" co-custom-component><span rv-html="counter" co-custom-component></span></co-slider>'
},{}]},{},[1]);

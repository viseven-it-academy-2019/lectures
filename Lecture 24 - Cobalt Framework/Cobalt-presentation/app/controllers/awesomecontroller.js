const nav = require('nav')

module.exports = function (scope) {
    // Write your awesome code here...

    scope.variableDefinedInController = 'This text is defined the controller!';
    scope.isVisible = true;

    scope.clickHandler = function (e) {
        scope.isVisible = !scope.isVisible
    };

    // nav.onwillgoto()
    nav.onenter(() => {
        console.log('localization model')
        console.log(scope.t)
        console.log('slide model')
        console.log(scope.m)
        console.log('common model')
        console.log(scope.m.common)
    })


    nav.onready(() => {
        alert('you are leaving the slide!')
    })
    // nav.onleave()


    scope.swipeUpHandler = function (e) {
        nav.nextSlide();
    }

    scope.swipeDownHandler = function (e) {
        nav.previousSlide()
    }

};
